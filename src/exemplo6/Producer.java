/**
 * Buffer
 * 
 * Autor: Lucio Agostinho Rocha
 * Ultima modificacao: 16/08/2017
 */
package exemplo6;

import java.util.Random;

public class Producer implements Runnable {

    private final static Random generator = new Random();
    private final Buffer buffer;
    
    public Producer( Buffer shared ){
        buffer = shared;
    }
    
    @Override
    public void run() {
        int sum=0;
        
        for (int count =1; count <=10; count++){
            
            try {
                //Dorme, atribui um valor no Buffer e soma
                Thread.sleep(generator.nextInt(3000));
                buffer.set( count );
                sum +=count; //incrementa a soma de valores
                System.out.printf("SomaProdutor:    %2d\n", sum);
            } catch ( InterruptedException e){
                e.printStackTrace();
            }
        }
                    System.out.println("Productor terminou a produção\nFinalizando o produtor");            
    }

    
    
}
