/**
 * Buffer
 * 
 * Autor: Lucio Agostinho Rocha
 * Ultima modificacao: 16/08/2017
 */
package exemplo6;

import java.util.Random;

public class Consumer implements Runnable {

    private final static Random generator = new Random();
    private final Buffer buffer;
    
    public Consumer( Buffer shared ){
        buffer = shared;
    }
    
    @Override
    public void run() {
        int sum=0;
        
        for (int count =1; count <=10; count++){
            
            try {
                //Dorme, adquire um valor do Buffer e soma ele (nao atribui nada no Buffer compartilhado)
                Thread.sleep(generator.nextInt(300));
                
                sum += buffer.get();
                System.out.printf("SomaConsumidor:  %2d\n", sum);
            } catch ( InterruptedException e){
                e.printStackTrace();
            }            
    }
        System.out.printf("\n%s %d\n%s\n",
                    "Consumidor leu valores totalizando: ", sum, "Finalizando consumidor");            
        }

    
    
}
